+++
title = "From Development to Production"
linkTitle = "Development to Production"
weight = 2
+++

## TL;DR
Different runtime environments require different tools.  
For local setups Docker and docker compose are the best fit,  
for dynamic or highly available setups Kubernetes is the best fit.

![devotion to duty](https://imgs.xkcd.com/comics/devotion_to_duty.png)

## Environments
Name | User | Visibility | Lifetime |  Purpose
--- | --- | --- | --- | ---
Development | Developers | Local | A few hours | rapid directly visible code changes
Test environment | Integration Tests | Internal | Duration of a build | Ensure implemented behavior does not degrade
Preview | Code reviewer / Product Owners / Customers | Branch lifetime | Internal / Protected | Discuss implementations / Review featues
Stage | Administrators | Internal / Protected | Project lifetime | Smoketest on a live-like as possible environment
Live | End users | Public | Project lifetime | Use the software like intended

## Container Managers
Property  | Docker | Docker Compose | Kubernetes | Ansible
--- | --- | --- | --- | ---
Manages  | Container | Groups of containers | Groups of groups of containers | Whole servers
Client Requirements | docker | docker-compose | kubectl | ansible-playbook
Server Requirements | Dockerd | Dockerd | Kubernetes Cluster | Python
Endpoint protocol | Docker Endpoint (REST) | Docker Endpoint (REST) | Kubernetes Apiserver (REST) | SSH Terminal
Packaging | Docker image | Docker image | Docker image | None (defines wished state, directly manages build artifacts)
Usecase | Manage containers on local mashines, remote Mashines or Docker Swarm | Manage containers on local mashines, remote mashines or Docker Swarm | Manage dynamic / scalable deployments | Manage bare metal Servers, shared Hosting, adopting legacy systems without automation
Best used in <td colspan=2> Local Development | Production | Situations that require flexibility
Links | [Documentation](https://docs.docker.com/engine/reference/run/) / [Github](https://github.com/docker/docker-ce) / [Marketing](https://www.docker.com) | [Documentation](https://docs.docker.com/compose/overview/) / [Github](https://github.com/docker/compose) | [Documentation](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) / [Github](https://github.com/kubernetes/kubernetes) / [Marketing](https://kubernetes.io/) | [Documentation](http://docs.ansible.com/ansible/latest/index.html) / [Github](https://github.com/ansible/ansible) / [Marketing](https://www.ansible.com/)
