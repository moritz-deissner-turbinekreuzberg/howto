+++
title = "Docker"
weight = 4
+++

## TL;DR
Docker is good for building container images and running local development setups.

![Docker](https://blog.dkd.de/content/images/2017/03/Docker-Logo.png)

## Name
Docker exists as the company, the companies product and the open source project (renamed to moby).  

## Hype
Docker was the first tool to provide simple access to Linux kernel features to create containers.  
It increased the speed of adoption for the movement towards smaller deployments.

![containers, containers everywhere](http://blog.cloud66.com/content/images/2015/05/56291573.jpg)

## Origin
- 1979 chroot
- 2000 FreeBSD jail
- 2005 OpenVZ
- 2007 cgroup in Linux kernel

## Technology
Docker uses Linux kernel features to create the illusion of containers to the user.  
Containers get isolated Filesystems, Process structures, Resource quotas, Network interfaces.  
Containers still share the Filesystem cache, Linux Kernel Version and Modules.  
For now quotas on open network connections, network throughput, disk iops and disk throughput is missing.

## Strucure
{{<mermaid align="left">}}
graph LR;
    A[docker] -->|REST| B[dockerd]
    B[dockerd] -->|System calls| C[Linux Kernel]
{{< /mermaid >}}

'docker' is a command line tool to interact with the 'dockerd' daemon.  
Dockerd interacts with the Linux Kernel.  
The command line tool is able to talk to a Docker daemon via the network.
