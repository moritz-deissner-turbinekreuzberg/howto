+++
title = "CICD Workflow"
weight = 7
+++

## TL;DR
Test exists to ensure currently added feature implementations do not reopen already finished tasks.  
Test get added during development.  
Branches only get merged when the automated Build succeeded.  

![say works on my machine](https://www.ybrikman.com/assets/img/blog/docker/say-works-on-my-machine.jpg)

## Workflow

{{<mermaid align="left">}}
graph LR;
    A[Local Devlopment] -->|1. git push| B[Branch]
    B[Branch] -->|becomes| C[Merge Request]
    C[Merge Request] -->|removes after merge| F[Preview Environment]
    B[Branch] -->|2. triggers| D[Automated Build]
    D[Automated Build] -->|3. builds| H[Container images]
    D[Automated Build] -->|4. runs| G[Fast tests]
    D[Automated Build] -->|11. shows test result| B[Branch]
    D[Automated Build] -->|5. creates| E[Test Environment]
    D[Automated Build] -->|6. tests| E[Test Environment]
    D[Automated Build] -->|7. removes| E[Test Environment]
    D[Automated Build] -->|9. optional: deploys| I[Stage]
    D[Automated Build] -->|10. optional: deploys| J[Live]
    D[Automated Build] -->|8. creates or updates| F[Preview Environment]
{{< /mermaid >}}

### Automated Builds
Every branch gets automatically build as a Gitlab CI Pipeline.

The actual build depends on the project and framework requirements.  
A Build is processed as a collection of Tasks.  
Tasks are grouped into stages. All tasks in one stage get executed in parallel.  
The pipeline stops, when a task fails. Tasks can be [retried](https://docs.gitlab.com/ce/ci/yaml/README.html#retry).  
The build process is defined in the [.gitlab-ci.yml](https://docs.gitlab.com/ce/ci/yaml/README.html#gitlab-ci-yml).

Tasks are a list of commands executed within a container.  
Before the commands are run, the runner uses git to clone the project.  
Tasks can create artifacts to archive files and transfer between tasks. Artifacts are send to gitlab and then to the next worker. Huge artifacts will seriously slow down the build.  

### Docker in Docker & Registry
The task runners are preconfigured to access  
a) the internal registry and  
b) a for container image building dedicated docker in docker deployment.

The domain of the cluster internal is stored in the system variable DOCKER_REGISTRY_INTERNAL. The default cluster setup uses 'registry.registry.svc'.  
Docker can be used like on a local mashine.


### Debugging
Builds should not depend on external, possible flaky, services.  
Or itself will become flaky.

Every build step happens within a Kubernetes pod and can be recreated and debugged like every other deployment.

Logs get automatically shipped to Gitlab.

### Tests

Tests help developers to not break features, they did not know about.  
Pretending a customers does not want tests to be written, is like pretending the customers says "it's ok, if the thing i paid you for, only works sometimes, or never."
Would you say that to the mechanic of your car?

#### Testers vs Developers

{{< tweet 943595646199717888 >}}

Automated tests are not written by testers.  
Automated tests are written by developers.  
Testers "hack" the software system until a new edgecase is found. And then they do it again.  
Testers do not create bugs, they help to find them.  

This [video](https://www.youtube.com/watch?v=ILkT_HV9DVU) (Youtube: Open Lecture by James Bach on Software Testing) shows the different mindset and skillset of Testers vs Developers.

### Most Stage and Live Environments

The Test environment and Preview environment are both deployed to Kubernetes.  
For Stage and Live deployments onto a shared hoster, vm or decidated server should happen using ansible.  

![docker in production](https://image.slidesharecdn.com/baruchsadogurskydockercontainerlifecyclesproblemoropportunity-160624201959/95/docker-container-lifecycles-problem-or-opportunity-by-baruch-sadogursky-jfrog-9-638.jpg)