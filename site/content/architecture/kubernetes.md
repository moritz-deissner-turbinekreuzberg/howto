+++
title = "Kubernetes"
weight = 5
+++

## TL;DR
Kubernetes combines resources from many nodes and provides automated management of deployments.

![Kubernetes](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Kubernetes_logo.svg/1024px-Kubernetes_logo.svg.png)

## Name
The word is greek and means 'steersman'.  
Sometimes Kubernetes is called k8s, because strlen('ubernete') == 8.

## Hype
Kubernetes defines a general purpose API to define computation.  
All big cloud provides offer Kubernetes as a service.  
It can be deployed on premisse.
It avoids vendor lock.  
The common API created a [ecosystem of software solutions](https://raw.githubusercontent.com/cncf/landscape/master/landscape/CloudNativeLandscape_latest.png) loosely governed by the cncf.  

![One does not simply "build" a Paas.](https://blogs.gartner.com/richard-watson/files/2015/05/one-does-not-simply-build-a-paas.jpg)

## Origin
Google has a lot of compute resources. They build internal systems to automatically deploy realtime and batch workloads onto the same mashines. That created a higher then usual saturation of hardware resources.

The latest google internal orchostration mamanger was called Borg [Paper](https://research.google.com/pubs/pub43438.html). Kubernetes is a opensource rewrite of Borg, while keeping the learnings from Borg.  

Kubernetes is owned by the [CNCF](https://www.cncf.io/) (as far as i know).

## Technology
Kubernetes is designed to have no single point of failure. Etcd is used as a distrubuted consistend datastore to keep the desired and real world state.

Every component asks in a loop the desired state and the real state. Changes need a few secounds to propagade through the system. When ever a component fails, the other components will keep the last known desired state deployed.

## Strucure
<div class="mxgraph" style="max-width:100%;border:1px solid transparent;" data-mxgraph="{&quot;highlight&quot;:&quot;#0000ff&quot;,&quot;nav&quot;:true,&quot;resize&quot;:true,&quot;toolbar&quot;:&quot;zoom layers lightbox&quot;,&quot;edit&quot;:&quot;_blank&quot;,&quot;xml&quot;:&quot;&lt;mxfile userAgent=\&quot;Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36\&quot; version=\&quot;7.8.8\&quot; editor=\&quot;www.draw.io\&quot; type=\&quot;browser\&quot;&gt;&lt;diagram id=\&quot;010a7908-4284-f35d-d739-17dad94cc331\&quot; name=\&quot;Page-1\&quot;&gt;3Ztdc5s4FIZ/DZedQUh8XSbebLvTTSdTX/SagmKzBeSRRezsr19hBEYfnSUEI9rexBxAiOc9ks45qA7clOePNDnsH0mGC8dzs7MD/3A8D8AA8D+N5bW1xK7fGnY0z8RFV8M2/xcLoyusdZ7ho3QhI6Rg+UE2pqSqcMokW0IpOcmXPZNCfuoh2WHNsE2TQrd+yzO2b62RF1ztn3C+23dPBkHcnvmepD92lNSVeJ7jwefLv/Z0mXRtiRc97pOMnAYm+ODADSWEtb/K8wYXDdsOW3vfnz852/eb4oqNucEX/XhJihp3Xb50jL12MC6vg5sbXAfen/Y5w9tDkjZnT1x+btuzsuBHgP98zotiQwpCL/d2Lw/vxWMwZfj8076CngD3LExKzOgrv0TcgKDAL5wKxaLzp6tEXoRa234gD+zcKhFusevbvqLhPwQdMykU/zqkgGuVVPALkQpdiVSkg+rhDUF57gygfI3T5/o7LjB7H645sHhQwuJ38/eAC/AMXIIZsOju02D58ETJ+dU6Gc+3SCbUyGxIxZK8wpSbv9YVy0tsHZHqPIGrD6qbIdJn6bunv7hhi+lLQ8kyG+gDiY23JBugL/YPLM1WBwUsCgUYBxUlRXEZVY9JxaM9+54Dosii53gapG26x1ldrICMD2ySgRqZv/OqPnPTZ0yrJi+y7TjKdBwa8PR5wOx4kIZHA4Kr7K7J2fhRWiTHY57KDP6py4NIEQN+2KdMbs8HZ1o6p9DhDyQ1TbG0jrKE7jAb6qhDHEAyMepsFBcJy1/kXpjAiSc8kZz376oRkjWKgC830fZe3DXM59SGIjnyD2JFxPadtYYuOvavPU5aPXx9s7TT5AvWJh+KFPniifL5UGkIKQ3NKN+ILO028vm6fKFN9aA6+FToY9WDsTLTqm4wn3qGOPfN6s0wkXbB01DL2KaWXizHAjBU5r/RWiKlIe9mEykcUYMzannkfWC3ljg2TLauY1FipCiD1FLMWImR6iv+7STWkwwbw9WkpWdVS6hIoMaco7VUsiOtkDmjlnouZENLQxBkdebtk/Y+hJ0oJfDlhkLVJ2aUUk/ebEhpCIjsSqkUxSfPsP1nrgVmWD3RXEsxHdksGUPzR4aVVNP9yCYaPQNaYzlddZ9Fy+lQ/+SwsvKWimfR8lYXvixc3houHtAQCHSqjaC4UJwXzlTf8sFi9S04Q4o9Rj/D6r86/SIwU4ErChYrcKGpafUb9UMG/fp9QivRL1DH39QSV6h+TLhdiQutI2c26Ws1OkdKfoSiidG5r2RsCN5sLkXeOrRcW6YVBIoEaoI0elyC/0nZZtRST5q/kIzHyW5H/KopD+uYoiKj5AfudnNVpMLKBi9hSop8VzWuwPXk4Ti8b4LEPE2KO3GizLOseYwxxJSDUOf9USbPieV5L9SjzMDT/UStGk/aT6entgK4Poh+G+A+sghcj+ofkyO7ZIUbUh44MM7o+PvCV/eamDbkRAb46gIyaVOkvuw3BYuUvTPVXHC7bV/FXiJD99extHayrWZpBZG8Ik7+Eth79exfAvnhdad/e/n1v1PAh/8A&lt;/diagram&gt;&lt;/mxfile&gt;&quot;}"></div>
<script type="text/javascript" src="https://www.draw.io/js/viewer.min.js"></script>