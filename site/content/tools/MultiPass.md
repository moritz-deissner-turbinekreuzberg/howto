+++
title = "MultiPass"
+++

[MultiPass](https://gilles.crettenand.info/MultiPass/) can be used to store passwords for wildcards locally within the browser.
