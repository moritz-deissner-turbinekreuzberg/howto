+++
title = "Kubectl"
+++

kubectl --context matrix get events --all-namespaces=true

`watch "kubectl top --all-namespaces=true pods | sort -k3 -h -r | head -n 30"`

`watch "kubectl top --all-namespaces=true pods | sort -k4 -h -r | head -n 30"`

edit yaml

{{< tweet 950856106728927232 >}}
