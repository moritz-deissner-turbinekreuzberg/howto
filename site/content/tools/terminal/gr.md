+++
title = "gr"
+++

Multiple git repository management tool

https://github.com/mixu/gr

for zsh run `unalias gr`, because gr is a an alias of the git plugin
