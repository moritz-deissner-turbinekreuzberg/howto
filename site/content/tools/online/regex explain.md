+++
title = "Explain Regex"
+++

[REGEXER](https://regexper.com/) explains Regex patterns.

example: https://regexper.com/#%5E%5Cd%7B4%7D%5B%5C%2F%5C-%5D(0%3F%5B1-9%5D%7C1%5B012%5D)%5B%5C%2F%5C-%5D(0%3F%5B1-9%5D%7C%5B12%5D%5B0-9%5D%7C3%5B01%5D)%24

![two problems...](http://s2.quickmeme.com/img/28/28267ccca83716ccddc3a2e194e8b0052cae3a204de3f37928a20e8ff4f0ee65.jpg)