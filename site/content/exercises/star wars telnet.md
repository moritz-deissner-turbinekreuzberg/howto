+++
title = "star wars telnet"
weight = 1
+++

## What STAR WARS via telnet

![STAR WARS](https://4bds6hergc-flywheel.netdna-ssl.com/wp-content/uploads/2015/12/Star-Wars-Linux-Terminal-5.png)

## Tasks

### 1. start the server

Use docker to run https://hub.docker.com/r/rohan/ascii-telnet-server/ and export port 21 (telnet) to your host.

### 2. watch STAR WARS

Use telnet to connect to the server

### 3. Exit telnet client

Use [Google](http://lmgtfy.com/?q=who+to+exit+telnet)

### 4. stop the server

Use the docker client to tell dockerd to stop the telnet server
