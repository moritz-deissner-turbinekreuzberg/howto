+++
title = "kubecraft"
weight = 4
+++

## Minecraft as a UI for Kubernetes

![kubecraft](https://github.com/eformat/kubecraft/raw/master/docs/img/kubecraft.png)

## Tasks

### 1. Play and have fun

Checkout https://github.com/eformat/kubecraft and have a look at kubernetes minecraft style
