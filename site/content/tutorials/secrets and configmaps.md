+++
title = "Secrets & Configmaps"
weight = 9
+++

Container environments get configured by setting system environment variables or placing configuration files in the right location.

System environment variables are simple to access via `kubectl describe` or `docker describe`.  
Files are simple to access via `kubectl cp` or `docker cp`.  

## Docker
The system variable CONFIGURATION gets set to "some string".
The local file /host/path/nginx.conf gets placed at the location /etc/nginx/nginx.conf inside the container.
```
docker run -e CONFIGURATION="some string" -v -v /host/path/nginx.conf:/etc/nginx/nginx.conf:ro -p 127.0.0.1:8080:80 nginx
```

## Docker-compose
The system variable CONFIGURATION gets set to "some string".
The local file /host/path/nginx.conf gets placed at the location /etc/nginx/nginx.conf inside the container.
```
version: '3.0'

services:
  webserver:
    image: nginx
    ports:
      - '127.0.0.1:8080:80'
    volumes:
      - "/host/path/nginx.conf:/etc/nginx/nginx.conf"
    environment:
      CONFIGURATION: "some string"
```

## Kubernetes

[Configmaps](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/) and [secrets](https://kubernetes.io/docs/concepts/configuration/secret/) can be used as lightweight volumes.  
These lightweight volumes are not persistent and can be mounted to multiple pods at the same time.  
The content of a secret lifes in the main memory of the worker nodes and never touches the disk.  

Secrets and configmaps can be defined in a yaml file or  
they can be created directly from the files they represent.

The [sealed secret](https://github.com/bitnami-labs/sealed-secrets) controller is able to provide a public key for encrypting secrets and automatically decrypts the secrets once they are transferred to kubernetes. It should be find to push those encrypted secrets to git.

### Configmap
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: hello-world
data:
  index.html: |
    <html>
        <head>
            <title>Hello, world!</title>
        </head>
        <body>
            Hello, world!
        </body>
    </html>
  username: admin
```
### Secret
secrets need to be base64 encoded
```
$ echo -n "1f2d1e2e67df" | base64
MWYyZDFlMmU2N2Rm
```
```
apiVersion: v1
kind: Secret
metadata:
  name: secure-place
type: Opaque
data:
  password: MWYyZDFlMmU2N2Rm
  tls.crt: ...
```

### Pod
```
apiVersion: v1
kind: Pod
metadata:
  name: some-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "env" ]
      env:
        - name: USERNAME
          valueFrom:
            configMapKeyRef:
              name: hello-world
              key: username
        - name: PASSWORD
          valueFrom:
            secretKeyRef:
                name: secure-place
                key: password
      volumeMounts:
        - name: html
          mountPath: /usr/share/nginx/html/index.html
          subPath: index.html
        - name: certificate
          mountPath: /etc/cert.crt
          subPath: tls.crt
  volumes:
    - name: html
      configMap:
        name: hello-world
    - name: certificate
      secret:
        secretName: secure-place
```

![secure secrets](https://i.imgflip.com/1p3oik.jpg)
