+++
title = "manage Kubernetes setup"
weight = 4
+++

## start
Kubernetes does not give the option to start something.  
It provides the ability to define a desired state and issues a start on it's own schedule (usually within secounds).  
```
kubectl apply -f state.yaml
```

A desired state can include the container deployment, services sending traffic, mapping domain names, cronjobs, secret values and a lot more.

## status
Kubernetes can list resources with `get` and show detail with `describe`.  
`kubectl get` lists all the resource types.

To get all running pods run
```
kubectl -n kube-system get po
```
The information can be extended with `-o wide`
```
kubectl -n kube-system get po -o wide
```
To get the desired state and the current status use `-o yaml`
```
kubectl -n kube-system get po etcd-node1 -o yaml
```
To get dtailed information for example about the scheduling, healthchecks or image downloads run
```
kubectl -n kube-system describe po etcd-node1
```

## restart
Pod can not be directly restarted, but deleting a pod with trigger its deployment state to recreate the element.
```
kubectl -n kube-system delete po kube-dns-b76db4f7f-v4fnh
```

## stop
To remove something delete its desired state manually
```
kubectl -n <ns> <resouce-type> <resource>
```
or delete its namespace (it nothing else is in this namespace)
```
kubectl -n <ns> <resouce-type> <resource>
```
or delete via the original definition
```
kubectl delete -f state.yaml
```

## rolling upgrade
Most resources can be overwritten with an updated desired state
```
kubectl apply -f state-v2.yaml
```
Kubernetes will start the new containers and only remove old container as new containers got started, if possible.  
For example database deployments can be configured to stop and then start the new version to avoid the possibility of two databases working on the same files.

![Kubernetes for everybody](https://s3.amazonaws.com/media-p.slid.es/uploads/730082/images/3945907/everybodygetsacluster.jpg)