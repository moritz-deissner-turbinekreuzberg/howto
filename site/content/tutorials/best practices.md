+++
title = "Best practices"
+++

## untilfail
Find and fix flaky tests  
save this as a script `untilfail`
```
#!/bin/bash
while "$@"; do :; done
```
run `untilfail make tests` before going to lunch

## create container images
How to [Dockerfile](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/)

## debug kubernetes deployments
Most common [Kubernetes problems](https://kukulinski.com/10-most-common-reasons-kubernetes-deployments-fail-part-1/)

## Chrome
increase shared memory  
https://github.com/elgalu/docker-selenium/issues/20#issuecomment-133011186    
https://docs.openshift.org/latest/dev_guide/shared_memory.html  
https://stackoverflow.com/questions/46085748/define-size-for-dev-shm-on-container-engine/46434614#46434614  
