+++
title = "view Logs"
weight = 6
+++

The log of a container is the output of the started process.  
If the application is only able to log to a file, or logs different events to different files,  
it is possible to use an additional container that read the logfile from a shared volume.

## Docker
To get the log output run
```
docker logs <container>
```
To follow the logs and keep printing event add `-f`.

## Docker-compose
To get the log output run
```
docker-compose logs <container>
```
To follow the logs and keep printing event add `-f`.  
To get logs from all containers ommit specifying `<container>`.

## Kubernetes
To get the log output run
```
kubectl -n <namespace> logs <pod> -c <container>
```
To follow the logs and keep printing event add `-f`.  
As long as the pod has only one container or the first listed container should be used `-c <container>` can be omitted.  
Additionally logs get accumulated to the EFK stack and are available via Kibana.  
The tool [Kail](https://github.com/boz/kail) allows simple following of groups of logs.  

![captains log](https://pics.me.me/captains-log-18778305.png)