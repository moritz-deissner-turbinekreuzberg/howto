+++
title = "Shell access"
weight = 6
+++

A shell (sh, bash, zsh) can be executed in every container, as any other process.  
Not every container has bash available.

To use nano it can be necessary to run `export TERM=xterm` within the shell.

## Docker
To get shell access run
```
docker exec -ti <container> sh
```
To have an interactive connection to the process `-ti` is needed.

## Docker-compose
To get shell access run
```
docker-compose exec <container> sh
```

## Kubernetes
To get shell access run
```
kubectl --namespace <namespace> exec -ti <pod> -c <container> sh
```
As long as the pod has only one container or the first listed container should be used `-c <container>` can be omitted.

![Ghost in the shell](https://pics.me.me/v-power-g-host-the-shell-i-n-19454233.png)
