+++
title = "execute a CronJob"
weight = 13
+++

A cronjob creates a job to be executes everytime the [crontab](http://www.adminschoice.com/crontab-quick-reference) matches the current time.

## Docker & docker-compose
There is no notion of a cronjob for docker or docker-compose.

It may be emulated via https://github.com/damoon/docker-cron.

## Kubernetes
A job creates a pod to execute the process.
```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            args:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure
```
The output is visible via the logs of the pod of the job.
```
kubectl get jobs -a --output=jsonpath={.items..metadata.name} | grep hello
kubectl logs -f $(kubectl get pods --show-all --selector=job-name=[jobname] --output=jsonpath={.items..metadata.name})
```

![debug email](http://www.commitstrip.com/wp-content/uploads/2013/04/Strips-CRON-550-finalenglish.jpg)