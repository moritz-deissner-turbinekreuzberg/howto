+++
title = "create Images"
weight = 5
+++


## Image Build
Docker uses Dockerfiles to define how to build container images.

Example: `docker build -t tag -f docker/release/Dockerfile .`  
This packs the current folder `.` and sends it to the dockerd endpoint.  
Dockerd will build a container image based on the Dockerfile `-f docker/release/Dockerfile`.  
The created image is remembered by its Tag `-t tag`.  
Files not needed for the build can be ignored via `.dockerignore`. This will speed up the build.  

Current [Best practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/) are listed in [Dockers documentation](https://docs.docker.com).

To list the existing images run `docker images`.  
To delete a existing image run `docker rmi <image name>`.

![Docker is building](https://i0.wp.com/www.developermemes.com/wp-content/uploads/2015/09/Slacking-Off-Excuse-is-Docker-Containers-Are-Building-Meme.png)

## Distribution
Container images can be pulled and pushed to Registries.  
The default Registry is [Docker Hub](https://hub.docker.com/).  
An alternative registry is [QUAY](https://quay.io). Every clound has its own registry to improve pull push network delays.  
The cluster internal registry is available as `$DOCKER_REGISTRY_INTERNAL`.

## Push/Push/Login
To access a none public registy it is necessary to authenticate
```
docker login <registry domain>
```

To push an local image to a new registy
```
docker tag <existing image> <registry name>/<new image name>
docker push <registry domain>/<new image name>
```

To pull an image from a regisrty
```
docker pull <registry domain>/<existing image name>
```


## Versioning
Every image has a version. The default version is `:latest`.  
The version is represented as a `:version` suffix to the image name.  
It is a good practive to use the hash of a git commit as the container version.  
`:latest` should only be used for local development.
