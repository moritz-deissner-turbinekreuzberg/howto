+++
title = "Incomming traffic"
weight = 8
+++

## Docker

Docker is able to configure its host to forward network traffic to a container.

```
docker run -ti -p 127.0.0.1:8080:80 nginx
```

## Docker-compose

Docker-compose is able to configure docker to forward network traffic to a container.

```
version: '3.0'

services:
  blog:
    image: wordpress:4.9.1-php7.2-apache
    ports:
      - '127.0.0.1:8080:80'
    links:
      - database:mysql
  database:
    image: mysql:8.0.3
    environment:
      MYSQL_ROOT_PASSWORD: password123
```

## Kubernetes

Kubernetes is able to forward traffic from the same port on all nodes to a internal service.  
That is used to forward port 80 (http) and 443 (https) to the nginx ingress controller.  
The Ingress controller reads from kubernetes the defined ingress resources and configures nginx.  
Nginx parses the HTTP headers and sends the requests to the service matching the domainname.

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: wordpress
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: wordpress.project.cluster.tld
    http:
      paths:
      - path: /
        backend:
          serviceName: wordpress
          servicePort: 80
```

Ingress resouces can be configured to force a password and use encryption.  
This automatically also adds a redirect from http to https.  
[MultiPass](https://gilles.crettenand.info/MultiPass/) can be used to store passwords for wildcards locally within the browser.

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: wordpress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
spec:
  tls:
  - hosts:
    - wordpress.project.cluster.tld
    secretName: project-wildcard-certificate
  rules:
  - host: wordpress.project.cluster.tld
    http:
      paths:
      - path: /
        backend:
          serviceName: wordpress
          servicePort: 80
```